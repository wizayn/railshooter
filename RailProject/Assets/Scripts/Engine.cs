﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Engine : MonoBehaviour
{
    public GameObject playerObj;
    public List<ScriptsEffects> effectsList;
    public List<ScriptMovement> movementList;
    public List<ScriptFacings> facingList;

    int currentWaypoint;
    public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
    public RotationAxes axes = RotationAxes.MouseXAndY;
    public float sensitivityX = 15F;
    public float sensitivityY = 15F;
    public float minimumX = -360F;
    public float maximumX = 360F;
    public float minimumY = -60F;
    public float maximumY = 60F;
    float rotationY = 0F;
    public Image splatterImage;
    public Image blackImage;
    public GameObject myHead;
    private Material empty;

    public int lastEditedMovement = 0, lastEditedFacing = 0, lastEditedEffect;

    void Start()
    {
        playerObj = GameObject.FindWithTag("Player");
        //splatterImage = GameObject.Find("SplatterCanvas").GetComponentInChildren<Image>();
        //splatterImage.enabled = false;
        //blackImage = GameObject.Find("BlackCanvas").GetComponentInChildren<Image>();
        //blackImage.enabled = false;
        myHead = GameObject.Find("Cube");
        StartCoroutine("MovementEngine");
        StartCoroutine("FacingEngine");
        StartCoroutine("EffectsEngine");
    }

    IEnumerator MovementEngine()
    {
        for (int i = 0; i < movementList.Count; i++)
        {
            switch(movementList[i].type)
            {
                case MovementTypes.STRAIGHT:
                    Debug.Log("Doing straight line movement");
                    yield return StartCoroutine(StraightLine(i));
                    break;
                case MovementTypes.WAIT:
                    Debug.Log("Movement Wait");
                    yield return StartCoroutine(MWait(i));
                    break;
                case MovementTypes.BEIZIER:
                    Debug.Log("Doing Bazier");
                    yield return StartCoroutine(BezierCurve(i));
                    break;
                case MovementTypes.ROTATEANDRETURN:
                    Debug.Log("Doing Rotate and Return");
                    yield return StartCoroutine(RotateAndReturn(i));
                    break;
            }
        }
    }
    IEnumerator FacingEngine()
    {
        for (int i = 0; i < facingList.Count; i++)
        {
            switch (facingList[i].type)
            {
                case FacingTypes.LOOKAT:
                    Debug.Log("Doing Looking at");
                    yield return StartCoroutine(LookAtTarget(i));
                    break;
                case FacingTypes.WAIT:
                    Debug.Log("Facing waiting");
                    yield return StartCoroutine(FWait(i));
                    break;
                case FacingTypes.FREELOOK:
                    Debug.Log("Free Look");
                    yield return StartCoroutine(FreeLook(i));
                    break;
                case FacingTypes.FACEDIRECTION:
                    Debug.Log("Face Direction");
                    yield return StartCoroutine(FaceDirection(i));
                    break;
            }
        }
    }

    IEnumerator EffectsEngine()
    {
        for (int i = 0; i < effectsList.Count; i++)
        {
            switch (effectsList[i].type)
            {
                case EffectTypes.SHAKE:
                    Debug.Log("Doing Looking at");
                    yield return StartCoroutine(CameraShake(i));
                    break;
                case EffectTypes.SPLATTER:
                    Debug.Log("Doing splatter");
                    yield return StartCoroutine(Splatter(i));
                    break;
                case EffectTypes.FADE:
                    Debug.Log("doing fading");
                    yield return StartCoroutine(Fade(i));
                    break;
                case EffectTypes.WAIT:
                    Debug.Log("Effects waiting");
                    yield return StartCoroutine(EWait(i));
                    break;
            }
        }
    }

    IEnumerator StraightLine(int waypointIndex)
    {
        Transform endPoint = movementList[waypointIndex].endPoint.transform;
        float moveTime = movementList[waypointIndex].speed;

        // Lerp along the line
        float elapsedTime = 0f;
        Vector3 startPoint = transform.position;

        while (elapsedTime < moveTime)
        {
            transform.position = Vector3.Lerp(startPoint, endPoint.position, (elapsedTime / moveTime));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }
    IEnumerator BezierCurve(int waypointIndex)
    {

        // Get current player position and rotation
        Vector3 beginningPosition = transform.position;

        Vector3 endingPosition = movementList[waypointIndex].bezierEndPoint.transform.position;
        Vector3 curvePosition = movementList[waypointIndex].bezierCurvePoint.transform.position;

        float startingTime = Time.time;
        float movementTime = movementList[waypointIndex].bezierMoveTime;
        float totalTime = 0.0f;

        while (Time.time < startingTime + movementTime)
        {
            totalTime += Time.deltaTime;
            float currentTime = totalTime / movementTime;
            transform.position = GetPoint(beginningPosition, endingPosition, curvePosition, currentTime);
            yield return new WaitForEndOfFrame();
        }

        transform.position = endingPosition;
    }
    public Vector3 GetPoint(Vector3 start, Vector3 end, Vector3 curve, float t)
    {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1.0f - t;
        return oneMinusT * oneMinusT * start + 2.0f * oneMinusT * t * curve + t * t * end;
    }
    IEnumerator MWait(int waypointIndex)
    {
        yield return new WaitForSeconds(movementList[waypointIndex].waitTime);
    }

    IEnumerator RotateAndReturn(int waypointIndex)
    {
        float elapsedTime = 0f;
        float time = movementList[waypointIndex].speed2;
        Vector3 lookPosition = movementList[waypointIndex].lookTarget.transform.position;
        Quaternion startRotation = playerObj.transform.rotation;
        Quaternion targetRotation = Quaternion.LookRotation((lookPosition - playerObj.transform.position).normalized);

        while (elapsedTime < time)
        {
            playerObj.transform.rotation = Quaternion.Lerp(startRotation, targetRotation, (elapsedTime / time));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        elapsedTime = 0;
        while (elapsedTime < time)
        {
            playerObj.transform.rotation = Quaternion.Lerp(targetRotation, Quaternion.Euler(playerObj.transform.forward), (elapsedTime / time));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        playerObj.transform.rotation = Quaternion.Euler(playerObj.transform.forward);
    }

    IEnumerator LookAtTarget(int waypointIndex)
    {
        float elapsedTime = 0f;
        float time = facingList[waypointIndex].speed;
        Vector3 lookPosition = facingList[waypointIndex].lookTarget.transform.position;
        Quaternion startRotation = Camera.main.transform.rotation;
        Quaternion targetRotation = Quaternion.LookRotation((lookPosition - Camera.main.transform.position).normalized);

        while (elapsedTime < time)
        {
            Camera.main.transform.rotation = Quaternion.Lerp(startRotation, targetRotation, (elapsedTime / time));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        elapsedTime = 0;
        while (elapsedTime < time)
        {
            Camera.main.transform.rotation = Quaternion.Lerp(targetRotation, Quaternion.Euler(Camera.main.transform.forward), (elapsedTime / time));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        Camera.main.transform.rotation = Quaternion.Euler(Camera.main.transform.forward);
    }

    IEnumerator FaceDirection(int waypointIndex)
    {
		/**
        float elapsedTime = 0f;
        float time = facingList[waypointIndex].speed;
		Vector3 lookPosition = facingList[waypointIndex].lookTarget.transform.position;
		Debug.Log ("I am Looking at" + facingList[waypointIndex].lookTarget.name);
		Quaternion startRotation = transform.rotation;
		Quaternion targetRotation = Quaternion.LookRotation((lookPosition - transform.position));
        Debug.Log("Before while loop");
        while (elapsedTime < time)
        {
            //Debug.Log("Face Direction running");
			transform.rotation = Quaternion.Slerp(startRotation, targetRotation, (elapsedTime / time));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        */
		float time = facingList [waypointIndex].speed;
		float eT = 0f;
		Quaternion from = transform.rotation;
		Transform to = (Transform)facingList[waypointIndex].lookTarget;

		while (eT < time) {
			transform.rotation = Quaternion.Slerp(transform.rotation, to.rotation , eT * time);
		}




    }
    IEnumerator FWait(int waypointIndex)
    {
        yield return new WaitForSeconds(facingList[waypointIndex].waitTime);
    }

    IEnumerator FreeLook(int waypointIndex)
    {
        float freeLookTime = facingList[waypointIndex].freeLookTime;
        float elapsedTime = 0f;
        while (elapsedTime < freeLookTime)
        {
            if (axes == RotationAxes.MouseXAndY)
            {
                float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;

                rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
                rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

                transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
            }
            else if (axes == RotationAxes.MouseX)
            {
                transform.Rotate(0, Input.GetAxis("Mouse X") * sensitivityX, 0);
            }
            else
            {
                rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
                rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

                transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0);
            }
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator CameraShake(int waypointIndex)
    {
        float startTime = Time.time;
        float shakingTime = effectsList[waypointIndex].duration;
        float intensity = effectsList[waypointIndex].intensity;

        while (Time.time < startTime + shakingTime)
        {
            //GetComponent<MeshRenderer>().enabled = false;
            myHead.transform.localPosition = Random.insideUnitSphere * intensity;
            yield return new WaitForEndOfFrame();
        }
        myHead.transform.localPosition = Vector3.zero;
        //GetComponent<MeshRenderer>().enabled = true;
    }

    // variables are 
    IEnumerator Fade(int waypointIndex)
    {
        float startTime = Time.time;
        blackImage.enabled = true;

        bool fadeInBlack = effectsList[waypointIndex].fadeInFromBlack;
        bool fadeOutBlack = effectsList[waypointIndex].fadeOutToBlack;
        float fadeInBlackTime = effectsList[waypointIndex].fadeInTime;
        float fadeOutBlackTime = effectsList[waypointIndex].fadeOutTime;

        if (fadeInBlack == true)
        {
            Color endColor = blackImage.color;
            endColor.a = 1f;

            float elapsedTime = 0f;

            Color startColor = blackImage.color;
            startColor.a = 0f;

            while (elapsedTime < fadeOutBlackTime)
            {
                Color tempColor = blackImage.color;

                tempColor.a = Mathf.Lerp(startColor.a, endColor.a, (elapsedTime / fadeOutBlackTime));
                blackImage.color = tempColor;

                elapsedTime += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }

            Color newTempColor = blackImage.color;
            newTempColor.a = 1f;
            blackImage.color = newTempColor;
        }


        if (fadeOutBlack == true)
        {
            Color endColor = blackImage.color;
            endColor.a = 0f;

            float elapsedTime = 0f;

            Color startColor = blackImage.color;
            startColor.a = 1f;

            while (elapsedTime < fadeInBlackTime)
            {
                Color tempColor = blackImage.color;

                tempColor.a = Mathf.Lerp(startColor.a, endColor.a, (elapsedTime / fadeInBlackTime));
                blackImage.color = tempColor;

                elapsedTime += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }

            Color newTempColor = blackImage.color;
            newTempColor.a = 0;
            blackImage.color = newTempColor;
        }

        blackImage.enabled = false;
    }



    IEnumerator Splatter(int waypointIndex)
    {
        float startTime = Time.time;
        float splatterDurationTime = effectsList[waypointIndex].splatterDuration;
        splatterImage.enabled = true;

        bool useFadeIn = effectsList[waypointIndex].useSplatterFadeIn;
        bool useFadeOut = effectsList[waypointIndex].useSplatterFadeOut;

        float fadeInTime = effectsList[waypointIndex].splatterFadeInTime;
        float fadeOutTime = effectsList[waypointIndex].splatterFadeOutTime;


        if (useFadeIn == true)
        {
            Color endColor = splatterImage.color;
            endColor.a = 1f;

            float elapsedTime = 0f;

            Color startColor = splatterImage.color;
            startColor.a = 0.0f;

            while (elapsedTime < fadeInTime)
            {
                Debug.Log("Fading in...");
                
                Color tempColor = splatterImage.color;

                tempColor.a = Mathf.Lerp(startColor.a, endColor.a, (elapsedTime / fadeInTime));
                Debug.Log(tempColor.a);

                splatterImage.color = tempColor;

                elapsedTime += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }

            Color newTempColor = splatterImage.color;
            newTempColor.a = 1;
            splatterImage.color = newTempColor;
        }

        //Turn on splatter image
        while (Time.time < startTime + splatterDurationTime)
        {
            splatterImage.enabled = true;
            yield return new WaitForEndOfFrame();
        }


        if (useFadeOut == true)
        {
            Color endColor = splatterImage.color;
            endColor.a = 0f;

            float elapsedTime = 0f;

            Color startColor = splatterImage.color;
            startColor.a = 1.0f;

            while (elapsedTime < fadeOutTime)
            {
                Color tempColor = splatterImage.color;

                tempColor.a = Mathf.Lerp(startColor.a, endColor.a, (elapsedTime / fadeOutTime));
                splatterImage.color = tempColor;

                elapsedTime += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }

            Color newTempColor = splatterImage.color;
            newTempColor.a = 0f;
            splatterImage.color = newTempColor;
        }

        splatterImage.enabled = false;


    }
    
    /*
     *Script gotten from wiki.unity3d.com 
    */
    IEnumerator UnderWater(int waypointIndex)
    {
        float startTime = Time.time;
        float elapsedTime = 0f;
        //sets the time under water
        float timeUnderwater = effectsList[waypointIndex].timeUnderwater;
        //default fog settings
        bool fog = RenderSettings.fog;
        //default fog color
        Color fogColor = RenderSettings.fogColor;
        //default fog density
        float fogDensity = RenderSettings.fogDensity;
        //the default skybox
        Material defaultSkybox = RenderSettings.skybox;
        //an empty material variable
        Material noSkybox = empty;
        //sets the background color
        Camera.main.backgroundColor = new Color(0, 0.4f, 0.7f, 1);
        //while start time is less then under water time it loops
        while(elapsedTime < timeUnderwater)
        {
            //renders the fog
            RenderSettings.fog = true;
            //sets the new fog color
            RenderSettings.fogColor = new Color(0, 0.4f, 0.7f, 0.6f);
            //sets the new fog density
            RenderSettings.fogDensity = 0.04f;
            //sets the skybox to a non-exsistant skybox
            RenderSettings.skybox = noSkybox;
            yield return new WaitForEndOfFrame();
            elapsedTime += Time.deltaTime;
        }
        //sets the fog back to the default
        RenderSettings.fog = fog;
        //sets the fog color back to default
        RenderSettings.fogColor = fogColor;
        //sets the fog density back to default
        RenderSettings.fogDensity = fogDensity;
        //sets the skybox back to default
        RenderSettings.skybox = defaultSkybox;
    }

    IEnumerator EWait(int waypointIndex)
    {
        yield return new WaitForSeconds(facingList[waypointIndex].waitTime);
    }
}
