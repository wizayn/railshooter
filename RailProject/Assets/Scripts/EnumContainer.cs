﻿public enum MovementTypes
{
    WAIT,
    STRAIGHT,
    BEIZIER,
    ROTATEANDRETURN
}

public enum FacingTypes
{
    WAIT,
    LOOKAT,
    FREELOOK,
    FACEDIRECTION
}

public enum EffectTypes
{
    WAIT,
    SHAKE,
    SPLATTER,
    FADE 
}

