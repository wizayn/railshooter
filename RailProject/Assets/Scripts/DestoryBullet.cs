﻿using UnityEngine;
using System.Collections;

public class DestoryBullet : MonoBehaviour
{
    public float destoryTime = 3.0f;
    public int enemyHealth = 5;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject, destoryTime);
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "Enemy")
        {            
            Destroy(gameObject);
        }
        
    }
}
