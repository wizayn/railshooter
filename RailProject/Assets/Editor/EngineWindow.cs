﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

public class EngineWindow : EditorWindow
{
    public List<ScriptsEffects> effectsList;
    public List<ScriptMovement> movementList;
    public List<ScriptFacings> facingList;

    Engine engineScript;

    int movementItem = 0;
    int facingItem = 0;
    int effectItem = 0;

    Vector2 minimumSize = new Vector2(750f, 350f);

    //setup button skins
    GUIStyle miniRight, miniCenter, miniLeft;

    //Position Variables
    float offsetX = 0f, offsetY = 0f, ELEMENT_HEIGHT = 17f;

    public static void Init()
    {
        EngineWindow window = ((EngineWindow)EngineWindow.GetWindow(typeof(EngineWindow)));
        window.Show();
    }

    void OnFocus()
    {
        //reference the script
        engineScript = GameObject.Find("EngineObject").GetComponent<Engine>();

        //BUTTONS
        miniRight = new GUIStyle(EditorStyles.miniButtonRight);
        miniLeft = new GUIStyle(EditorStyles.miniButtonLeft);
        miniCenter = new GUIStyle(EditorStyles.miniButtonMid);

        //Pull info from the script
        movementList = engineScript.movementList;
        effectsList = engineScript.effectsList;
        facingList = engineScript.facingList;

        movementItem = engineScript.lastEditedMovement;
        facingItem = engineScript.lastEditedFacing;
        effectItem = engineScript.lastEditedEffect;

        //Make sure nothing is null
        if (movementList == null)
            movementList = new List<ScriptMovement>();
        if (effectsList == null)
            effectsList = new List<ScriptsEffects>();
        if (facingList == null)
            facingList = new List<ScriptFacings>();

        //Make sure that there is at least one element in the list
        if (movementList.Count <= 0)
            movementList.Add(new ScriptMovement());
        if (effectsList.Count <= 0)
            effectsList.Add(new ScriptsEffects());
        if (facingList.Count <= 0)
            facingList.Add(new ScriptFacings());
    }

    void OnGUI()
    {
        //built in variable for editor window
        minSize = minimumSize;
        offsetY = 0f;
        MovementGUI();
        EffectsGUI();
        FacingsGUI();
    }

    void MovementGUI()
    {
        offsetX = 0;
        offsetY = 0;
        //sets up rects for showing info
        Rect movementLabel = new Rect(45f, 10f, 250f, ELEMENT_HEIGHT);
        offsetY += 10f + ELEMENT_HEIGHT;
        //Setup to draw a line
        Vector2 linePointTop = new Vector2(229, 8);
        Vector2 linePointBottom = new Vector2(229, 195);
        Drawing.DrawLine(linePointTop, linePointBottom, Color.green, 1f, true);

        EditorGUI.LabelField(movementLabel, "MOVEMENT WAYPOINTS");

        Rect numberLabel = new Rect(90f, offsetY, 200f, ELEMENT_HEIGHT);
        EditorGUI.LabelField(numberLabel, ((movementItem + 1) + " / " + movementList.Count));
        offsetY += ELEMENT_HEIGHT + 2f;
        //Waypoint Name
        Rect nameLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
        //offsetY += ELEMENT_HEIGHT;
        EditorGUI.LabelField(nameLabel, "Waypoint Name: ");

        Rect nameInput = new Rect(offsetX + 100f, offsetY, 115f, ELEMENT_HEIGHT);
        offsetY += ELEMENT_HEIGHT;
        movementList[movementItem].name = EditorGUI.TextField(nameInput, movementList[movementItem].name);

        Rect typeRect = new Rect(offsetX, offsetY, 220f, ELEMENT_HEIGHT);
        offsetY += ELEMENT_HEIGHT + 2f;
        offsetX = 0f;
        movementList[movementItem].type = (MovementTypes)EditorGUI.EnumPopup(typeRect, movementList[movementItem].type);

        //Specific Display Info
        switch(movementList[movementItem].type)
        {
            case MovementTypes.STRAIGHT:
                Rect startLabel = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                offsetX += 100f;
                EditorGUI.LabelField(startLabel, "End Point: ");

                Rect endRect = new Rect(offsetX, offsetY, 115f, ELEMENT_HEIGHT);
                offsetX = 0f;
                movementList[movementItem].endPoint = (GameObject)EditorGUI.ObjectField(endRect, movementList[movementItem].endPoint, typeof(GameObject), true);
                offsetY += ELEMENT_HEIGHT + 2;
                startLabel = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                offsetX += 125f;
                EditorGUI.LabelField(startLabel, "Seconds to move: ");
                startLabel = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX += 30f;
                movementList[movementItem].speed = EditorGUI.FloatField(startLabel, movementList[movementItem].speed);
                break;
            case MovementTypes.BEIZIER:
                Rect startLabel2 = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                offsetX += 100f;
                EditorGUI.LabelField(startLabel2, "End Point: ");

                Rect endRect2 = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                offsetX = 0f;
                movementList[movementItem].bezierEndPoint = (GameObject)EditorGUI.ObjectField(endRect2, movementList[movementItem].bezierEndPoint, typeof(GameObject), true);
                offsetY += ELEMENT_HEIGHT + 2;
                startLabel2 = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                //Rect startLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX += 100f;
                EditorGUI.LabelField(startLabel2, "Curve Point: ");

                //Rect endRect2 = new Rect(offsetX, offsetY, 115f, ELEMENT_HEIGHT);
                endRect2 = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                offsetX = 0f;
                movementList[movementItem].bezierCurvePoint = (GameObject)EditorGUI.ObjectField(endRect2, movementList[movementItem].bezierCurvePoint, typeof(GameObject), true);
                offsetY += ELEMENT_HEIGHT + 2;
                startLabel2 = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                offsetX += 125f;
                EditorGUI.LabelField(startLabel2, "Seconds to move: ");
                startLabel2 = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX += 30f;
                movementList[movementItem].bezierMoveTime = EditorGUI.FloatField(startLabel2, movementList[movementItem].bezierMoveTime);
                break;
            case MovementTypes.WAIT:
                Rect startLabel3 = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                offsetX += 125f;
                EditorGUI.LabelField(startLabel3, "Seconds to wait: ");
                startLabel3 = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                movementList[movementItem].waitTime = EditorGUI.FloatField(startLabel3, movementList[movementItem].waitTime);
                break;
            case MovementTypes.ROTATEANDRETURN:
                startLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX += 100f;
                EditorGUI.LabelField(startLabel, "Look at target: ");
                
                startLabel = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                offsetX -= 100f;
                offsetY += ELEMENT_HEIGHT;
                movementList[movementItem].lookTarget = (GameObject)EditorGUI.ObjectField(startLabel, movementList[movementItem].lookTarget, typeof(GameObject), true);

                startLabel = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                offsetX += 135f;
                EditorGUI.LabelField(startLabel, "Route to target over: ");
                startLabel = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX += 30f;
                movementList[movementItem].speed2 = EditorGUI.FloatField(startLabel, movementList[movementItem].speed2);

                startLabel = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX = 35f;
                offsetY += ELEMENT_HEIGHT;
                EditorGUI.LabelField(startLabel, "secs");
                break;
        }

        //Display Buttons

        //locations for buttons
        Rect prevButtonRect = new Rect(50f, 175f, 40f, ELEMENT_HEIGHT);
        if (movementItem != 0)
        {
            if (GUI.Button(prevButtonRect, "Prev", miniLeft))
            {
                //Debug.Log("Hit previous");
                movementItem--;
            }
        }
        Rect addButtonRect = new Rect(90f, 175f, 40f, ELEMENT_HEIGHT);
        if (GUI.Button(addButtonRect, "Add", miniCenter))
        {
            Debug.Log("Hit add");
            movementList.Insert(movementItem + 1, new ScriptMovement());
        }
        Rect nextButtonRect = new Rect(130f, 175f, 40f, ELEMENT_HEIGHT);
        if (GUI.Button(nextButtonRect, "Next", miniRight))
        {
            Debug.Log("Hit next");
            if (movementItem < movementList.Count - 1)
                movementItem++;
            else
            {
                movementList.Add(new ScriptMovement());
                movementItem++;
            }
        }

        if (movementList.Count > 1)
        {
            Rect deleteRect = new Rect(60f, 200f, 110f, ELEMENT_HEIGHT);
            GUI.color = Color.red;
            if (GUI.Button(deleteRect, "Delete Waypoint"))
            {
                if (movementItem == movementList.Count - 1)
                {
                    movementList.RemoveAt(movementItem);
                    movementItem--;
                }
                else if (movementList.Count > 1)
                {
                    movementList.RemoveAt(movementItem);
                }
                else
                {
                    Debug.Log("Cannot delete only waypoint");
                }
            }
        }

        GUI.color = Color.white;
    }
    void EffectsGUI()
    {
        offsetX = 0;
        offsetY = 0;
        //sets up rects for showing info
        Rect effectsLabel = new Rect(545f, 10f, 250f, ELEMENT_HEIGHT);
        offsetY += 10f + ELEMENT_HEIGHT;
        //Setup to draw a line
        Vector2 linePointTop = new Vector2(750, 8);
        Vector2 linePointBottom = new Vector2(750, 195);
        Drawing.DrawLine(linePointTop, linePointBottom, Color.green, 1f, true);

        EditorGUI.LabelField(effectsLabel, "EFFECTS WAYPOINTS");

        //effects

        Rect numberLabel = new Rect(590f, offsetY, 200f, ELEMENT_HEIGHT);
        EditorGUI.LabelField(numberLabel, ((effectItem + 1) + " / " + effectsList.Count));
        offsetY += ELEMENT_HEIGHT + 2f;
        effectsLabel = new Rect(520f, offsetY, 100f, ELEMENT_HEIGHT);
        offsetX += 140;
        EditorGUI.LabelField(effectsLabel, "Effect Type: ");

        effectsLabel = new Rect(620f, offsetY, 100f, ELEMENT_HEIGHT);
        offsetX = 520f;
        offsetY += ELEMENT_HEIGHT + 2f;
        effectsList[effectItem].name = EditorGUI.TextField(effectsLabel, effectsList[effectItem].name);

        offsetX = 520f;
        effectsLabel = new Rect(offsetX, offsetY, 200f, ELEMENT_HEIGHT);
        effectsList[effectItem].type = (EffectTypes)EditorGUI.EnumPopup(effectsLabel, effectsList[effectItem].type);

        offsetY += ELEMENT_HEIGHT;
        switch (effectsList[effectItem].type)
        {
            case EffectTypes.SHAKE:
                effectsLabel = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                offsetX += 125f;
                EditorGUI.LabelField(effectsLabel, "Duration: ");
                effectsLabel = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX += 30f;
                effectsList[effectItem].duration = EditorGUI.FloatField(effectsLabel, effectsList[effectItem].duration);
                offsetY += ELEMENT_HEIGHT + 2;
                offsetX = 520;
                effectsLabel = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                offsetX += 125f;
                EditorGUI.LabelField(effectsLabel, "Intensity: ");
                effectsLabel = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX += 30f;
                effectsList[effectItem].intensity = EditorGUI.FloatField(effectsLabel, effectsList[effectItem].intensity);
                break;
            case EffectTypes.SPLATTER:
                offsetY -= 2;
                effectsLabel = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                offsetX += 170f;
                EditorGUI.LabelField(effectsLabel, "Splatter Duration: ");
                effectsLabel = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX += 70f;
                effectsList[effectItem].splatterDuration = EditorGUI.FloatField(effectsLabel, effectsList[effectItem].splatterDuration);
                offsetY += ELEMENT_HEIGHT;
                offsetX = 520;
                effectsLabel = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                offsetX += 170f;
                EditorGUI.LabelField(effectsLabel, "Use Splatter Fade in?");
                effectsLabel = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX += 30f;
                effectsList[effectItem].useSplatterFadeIn = EditorGUI.Toggle(effectsLabel, effectsList[effectItem].useSplatterFadeIn);
                offsetY += ELEMENT_HEIGHT;
                offsetX = 520;
                effectsLabel = new Rect(offsetX, offsetY, 150f, ELEMENT_HEIGHT);
                offsetX += 170f;
                EditorGUI.LabelField(effectsLabel, "Use Splatter Fade out?");
                effectsLabel = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX += 30f;
                effectsList[effectItem].useSplatterFadeOut = EditorGUI.Toggle(effectsLabel, effectsList[effectItem].useSplatterFadeOut);
                offsetY += ELEMENT_HEIGHT;
                offsetX = 520;
                effectsLabel = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                offsetX += 170f;
                EditorGUI.LabelField(effectsLabel, "Fade in Time: ");
                effectsLabel = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX += 30f;
                effectsList[effectItem].splatterFadeInTime = EditorGUI.FloatField(effectsLabel, effectsList[effectItem].splatterFadeInTime);
                offsetY += ELEMENT_HEIGHT;
                offsetX = 520;
                effectsLabel = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                offsetX += 170f;
                EditorGUI.LabelField(effectsLabel, "Fade out Time: ");
                effectsLabel = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX += 30f;
                effectsList[effectItem].splatterFadeOutTime = EditorGUI.FloatField(effectsLabel, effectsList[effectItem].splatterFadeOutTime);
                break;
            case EffectTypes.FADE:
                effectsLabel = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                offsetX += 165f;
                EditorGUI.LabelField(effectsLabel, "Fade in from Black?");
                
                effectsLabel = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX += 50f;
                effectsList[effectItem].fadeInFromBlack = EditorGUI.Toggle(effectsLabel, effectsList[effectItem].fadeInFromBlack);

                offsetY += ELEMENT_HEIGHT + 2;
                offsetX = 520;
                effectsLabel = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                offsetX += 125f;
                EditorGUI.LabelField(effectsLabel, "Fade out to Black?");
                offsetX += 40f;
                effectsLabel = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX += 40f;
                effectsList[effectItem].fadeOutToBlack = EditorGUI.Toggle(effectsLabel, effectsList[effectItem].fadeOutToBlack);
                offsetY += ELEMENT_HEIGHT + 2;
                offsetX = 520;
                effectsLabel = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                offsetX += 160f;
                EditorGUI.LabelField(effectsLabel, "Fade in Time: ");
                effectsLabel = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX += 60f;
                effectsList[effectItem].fadeInTime = EditorGUI.FloatField(effectsLabel, effectsList[effectItem].fadeInTime);
                offsetY += ELEMENT_HEIGHT + 2;
                offsetX = 520;
                effectsLabel = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                offsetX += 160f;
                EditorGUI.LabelField(effectsLabel, "Fade out Time: ");
                effectsLabel = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                
                effectsList[effectItem].fadeOutTime = EditorGUI.FloatField(effectsLabel, effectsList[effectItem].fadeOutTime);
                break;
            case EffectTypes.WAIT:
                Rect startLabel = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                offsetX += 125f;
                EditorGUI.LabelField(startLabel, "Seconds to wait: ");
                startLabel = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX += 30f;
                effectsList[effectItem].waitTime = EditorGUI.FloatField(startLabel, effectsList[effectItem].waitTime);
                break;
        }

    Rect prevButtonRect = new Rect(550f, 175f, 40f, ELEMENT_HEIGHT);
        if (effectItem != 0)
        {
            if (GUI.Button(prevButtonRect, "Prev", miniLeft))
            {
                //Debug.Log("Hit previous");
                effectItem--;
            }
        }
        Rect addButtonRect = new Rect(590f, 175f, 40f, ELEMENT_HEIGHT);
        if (GUI.Button(addButtonRect, "Add", miniCenter))
        {
            Debug.Log("Hit add");
            effectsList.Insert(effectItem + 1, new ScriptsEffects());
        }
        Rect nextButtonRect = new Rect(630f, 175f, 40f, ELEMENT_HEIGHT);
        if (GUI.Button(nextButtonRect, "Next", miniRight))
        {
            Debug.Log("Hit next");
            if (effectItem < effectsList.Count - 1)
                effectItem++;
            else
            {
                effectsList.Add(new ScriptsEffects());
                effectItem++;
            }
        }

        
        if (effectsList.Count > 1)
        {
            Rect deleteRect = new Rect(560f, 200f, 110f, ELEMENT_HEIGHT);
            GUI.color = Color.red;
            if (GUI.Button(deleteRect, "Delete Waypoint"))
            {
                if (effectItem == effectsList.Count - 1)
                {
                    effectsList.RemoveAt(effectItem);
                    effectItem--;
                }
                else if (effectsList.Count > 1)
                {
                    effectsList.RemoveAt(effectItem);
                }
                else
                {
                    Debug.Log("Cannot delete only waypoint");
                }
            }
        }

        GUI.color = Color.white;
    }
    void FacingsGUI()
    {
        offsetX = 0;
        offsetY = 0;
        //sets up rects for showing info
        Rect windowDisplay = new Rect(295f, 10f, 250f, ELEMENT_HEIGHT);
        offsetY = 10f;
        //Setup to draw a line
        Vector2 linePointTop = new Vector2(500, 8);
        Vector2 linePointBottom = new Vector2(500, 195);
        Drawing.DrawLine(linePointTop, linePointBottom, Color.green, 1f, true);

        EditorGUI.LabelField(windowDisplay, "FACINGS WAYPOINTS");

        //facing
        offsetY += ELEMENT_HEIGHT;
        Rect numberLabel = new Rect(340f, offsetY, 200f, ELEMENT_HEIGHT);
        EditorGUI.LabelField(numberLabel, ((facingItem + 1) + " / " + facingList.Count));
        offsetY += ELEMENT_HEIGHT + 2f;
        windowDisplay = new Rect(250f, offsetY, 100f, ELEMENT_HEIGHT);
        offsetX += 100;
        EditorGUI.LabelField(windowDisplay, "Facing Type: ");

        windowDisplay = new Rect(350f, offsetY, 100f, ELEMENT_HEIGHT);
        offsetX = 250f;
        offsetY += ELEMENT_HEIGHT + 2f;
        facingList[facingItem].name = EditorGUI.TextField(windowDisplay, facingList[facingItem].name);

        offsetX = 250f;
        windowDisplay = new Rect(offsetX, offsetY, 200f, ELEMENT_HEIGHT);
        facingList[facingItem].type = (FacingTypes)EditorGUI.EnumPopup(windowDisplay, facingList[facingItem].type);

        offsetY += ELEMENT_HEIGHT;
        switch(facingList[facingItem].type)
        {
            case FacingTypes.LOOKAT:
                windowDisplay = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX += 90f;
                EditorGUI.LabelField(windowDisplay, "Look at target");

                windowDisplay = new Rect(offsetX, offsetY, 110f, ELEMENT_HEIGHT);
                offsetX = 250;
                offsetY += ELEMENT_HEIGHT;
                facingList[facingItem].lookTarget = (GameObject)EditorGUI.ObjectField(windowDisplay, facingList[facingItem].lookTarget, typeof(GameObject), true);

                windowDisplay = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                offsetX += 135f;
                EditorGUI.LabelField(windowDisplay, "Route to target over: ");
                windowDisplay = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX += 30f;
                facingList[facingItem].speed = EditorGUI.FloatField(windowDisplay, facingList[facingItem].speed);

                windowDisplay = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX = 285f;
                offsetY += ELEMENT_HEIGHT;
                EditorGUI.LabelField(windowDisplay, "secs");
                break;
            case FacingTypes.FREELOOK:
                windowDisplay = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                offsetX += 135f;
                EditorGUI.LabelField(windowDisplay, "Seconds to free look: ");
                windowDisplay = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX += 30f;
                facingList[facingItem].freeLookTime = EditorGUI.FloatField(windowDisplay, facingList[facingItem].freeLookTime);
                break;
            case FacingTypes.WAIT:
                Rect startLabel3 = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                offsetX += 125f;
                EditorGUI.LabelField(startLabel3, "Seconds to wait: ");
                startLabel3 = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX += 30f;
                facingList[facingItem].waitTime = EditorGUI.FloatField(startLabel3, facingList[facingItem].waitTime);
                break;
		case FacingTypes.FACEDIRECTION:
			windowDisplay = new Rect (offsetX, offsetY, 100f, ELEMENT_HEIGHT);
			offsetX += 110f;
			EditorGUI.LabelField (windowDisplay, "Look at target");

                windowDisplay = new Rect(offsetX, offsetY, 110f, ELEMENT_HEIGHT);
                offsetX -= 110f;
                offsetY += ELEMENT_HEIGHT;
                facingList[facingItem].lookTarget = (GameObject)EditorGUI.ObjectField(windowDisplay, facingList[facingItem].lookTarget, typeof(GameObject), true);

                windowDisplay = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                
                EditorGUI.LabelField(windowDisplay, "Speed to look: ");
                offsetX += 110f;
                windowDisplay = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                facingList[facingItem].speed = EditorGUI.FloatField(windowDisplay, facingList[facingItem].speed);
                break;
        }



        Rect prevButtonRect = new Rect(300f, 175f, 40f, ELEMENT_HEIGHT);
        if (facingItem != 0)
        {
            if (GUI.Button(prevButtonRect, "Prev", miniLeft))
            {
                //Debug.Log("Hit previous");
                facingItem--;
            }
        }
        Rect addButtonRect = new Rect(340f, 175f, 40f, ELEMENT_HEIGHT);
        if (GUI.Button(addButtonRect, "Add", miniCenter))
        {
            Debug.Log("Hit add");
            facingList.Insert(facingItem + 1, new ScriptFacings());
        }
        Rect nextButtonRect = new Rect(380f, 175f, 40f, ELEMENT_HEIGHT);
        if (GUI.Button(nextButtonRect, "Next", miniRight))
        {
            Debug.Log("Hit next");
            if (facingItem < facingList.Count - 1)
                facingItem++;
            else
            {
                facingList.Add(new ScriptFacings());
                facingItem++;
            }
        }

        if (facingList.Count > 1)
        {
            Rect deleteRect = new Rect(310f, 200f, 110f, ELEMENT_HEIGHT);
            GUI.color = Color.red;
            if (GUI.Button(deleteRect, "Delete Waypoint"))
            {
                if (facingItem == facingList.Count - 1)
                {
                    facingList.RemoveAt(facingItem);
                    facingItem--;
                }
                else if (facingList.Count > 1)
                {
                    facingList.RemoveAt(facingItem);
                }
                else
                {
                    Debug.Log("Cannot delete only waypoint");
                }
            }
        }

        GUI.color = Color.white;
    }

    void OnLostFocus()
    {
        PushData();
    }

    void PushData()
    {
        engineScript.movementList = movementList;
        engineScript.effectsList = effectsList;
        engineScript.facingList = facingList;
        engineScript.lastEditedMovement = movementItem;
        engineScript.lastEditedFacing = facingItem;
    }
}
